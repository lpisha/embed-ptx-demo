# Copyright (C) 2020 NVIDIA Corporation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Compiles a CUDA source file to PTX, and creates a C file containing a
# character array of the text of the PTX. Uses the oldest (lowest-numbered)
# architecture set in CMAKE_CUDA_ARCHITECTURES. If this is not set, it will use
# the NVCC default.
# @param c_embed_name This argument serves two functions.
# First, it is the name of the character array containing the PTX source.
# Second, a CMake variable by this name will be set by the function, containing
# the path to the generated C file.
# @param cu_file The literal name of the input CUDA file, relative to the
# current source directory.
# Example:
# embed_ptx(myPtxProgram my_program.cu)
# add_executable(foo a.c b.cu c.cpp ${myPtxProgram})
# WARNING: Your CMake top-level project MUST include the C language, otherwise
# CMake will never emit rules to build the generated C file into an object file
# and you will get "undefined reference to myPtxProgram" from the linker.

function(embed_ptx c_embed_name cu_file)
    set(ptxfile "${CMAKE_CURRENT_BINARY_DIR}/${c_embed_name}.ptx")
    set(embedfile "${CMAKE_CURRENT_BINARY_DIR}/${c_embed_name}.c")
    file(RELATIVE_PATH cu_file ${CMAKE_CURRENT_BINARY_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/${cu_file})
    if(NOT BIN2C)
        get_filename_component(cudabindir ${CMAKE_CUDA_COMPILER} DIRECTORY)
        if(WIN32)
            set(BIN2C "${cudabindir}/bin2c.exe")
        else()
            set(BIN2C "${cudabindir}/bin2c")
        endif()
        if(NOT EXISTS ${BIN2C})
            message(FATAL_ERROR "Could not find bin2c at ${BIN2C}!")
        endif()
    endif()
    if(CMAKE_CUDA_ARCHITECTURES)
        set(archflags ${CMAKE_CUDA_ARCHITECTURES})
        list(SORT archflags CASE INSENSITIVE)
        list(GET archflags 0 archflags)
        string(REPLACE "-real" "" archflags ${archflags})
        string(REPLACE "-virtual" "" archflags ${archflags})
        set(archflags "-gencode arch=compute_${archflags},code=compute_${archflags}")
    else()
        set(archflags "")
    endif()
    set(allcudaflags "${CMAKE_CUDA_FLAGS} ${archflags}")
    separate_arguments(allcudaflags)
    add_custom_command(
        OUTPUT ${embedfile}
        COMMAND ${CMAKE_CUDA_COMPILER} ${allcudaflags} -ptx ${cu_file} -o ${ptxfile}
        COMMAND ${BIN2C} -c --padd 0 --type char --name ${c_embed_name} ${ptxfile} > ${embedfile}
        DEPENDS ${cu_file}
        COMMENT "Building embedded PTX source ${embedfile}"
        VERBATIM
    )
    set(${c_embed_name} ${embedfile} PARENT_SCOPE)
endfunction()
