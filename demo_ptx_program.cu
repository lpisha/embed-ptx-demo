__global__ void demoPtxKernel(float *a, const float *b, const float *c){
    a[threadIdx.x] += b[blockIdx.x] * c[threadIdx.x];
}
