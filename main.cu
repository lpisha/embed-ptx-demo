#include <stdio.h>

extern "C" const unsigned char demoPtxProgram[];

__global__ void testKernel(){
    printf("Hello world from device!\n");
}

int main(){
    testKernel<<<1,1>>>();
    cudaDeviceSynchronize();
    printf("Embedded PTX: \n%s\n", demoPtxProgram);
    return 0;
}
